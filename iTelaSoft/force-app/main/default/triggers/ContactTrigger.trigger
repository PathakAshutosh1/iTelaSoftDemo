/*
    Purpose			: This is a Trigger on Contact Object to write customization for DML's.
    Author			: Ashutosh Pathak
    Created Date	: 7 Feb 2023
	Created For		: iTelaSoft Case Study
    Version			: 1: Initial Draft 
    */
trigger ContactTrigger on Contact (after update) {
	ContactTriggerHandler.afterUpdate(Trigger.newMap, Trigger.oldMap);
}