public class ContactTriggerHandler {
    static String CustomerType = 'Customer';
    public static void afterUpdate(Map<Id, Contact> newMap, Map<Id, Contact> oldMap){
        processHasUpdateFunctionality(newMap, oldMap);
    }
    
    //This method takes the contact details and find all the acccounts associated and handle field update of Has_Customer__c
    private static void processHasUpdateFunctionality(Map<Id, Contact> newMap, Map<Id, Contact> oldMap){
        try{
            Set<Id> accountIds = new Set<Id>();
            Map<Id,Account> accounts = new Map<Id, Account>();
            for(Contact con: newMap.values()){
                accountIds.add(con.AccountId);
                accounts.put(con.AccountId, new Account(Id=con.AccountId, Has_Customer__c = false));
            }
            
            //Aggregate query is written because it will help setting the valid status of account based on account search
            //if code is written to use only contacts affected in DML, there is a possibility that false flag is set if Contact is not customer.
            //There are possibility if Account 'A' has 20 contacts where 10 are of Type Customers' and out of those 10, only 1 contact is being used in DML with
            //changed value of Contact_Type__c. If we dont consider aggregate query and use the contact found in DML. We might end up setting false flag which should not be correct.
            //This is the reason aggreagtion is used.
            for(AggregateResult acc: [SELECT AccountId, Count(Id)Total FROM Contact WHERE Contact_Type__c =:CustomerType AND AccountId=:accountIds GROUP BY AccountId ]){
                system.debug('##acc: '+acc);
                Boolean hasCustomer = ((Integer) acc.get('Total')) > 0;
                Id accountId = (Id)acc.get('AccountId');
                accounts.put(accountId, new Account(Id=accountId, Has_Customer__c = hasCustomer));
            }
            system.debug('##accounts: '+accounts);
            if(accounts.isEmpty())
                return;
            
            UPDATE accounts.values();
        }catch(Exception e){
            //TODO: Exception Loggin mechanism
            system.debug('##ERROR: '+e);
        }
    }
}