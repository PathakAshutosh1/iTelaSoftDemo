@isTest
public class ContactTriggerHandlerTest {
    @testSetup
    static void setup(){
        Account acc = new Account(Name='Wayne Enterprises');
        INSERT acc;
        List<Contact> contacts = new List<Contact>();
        contacts.add(new Contact(LastName='Bruce Wayne', Contact_Type__c = 'Non-Customer', AccountId=acc.Id));
        contacts.add(new Contact(LastName='Martha Wayne', Contact_Type__c = 'Non-Customer', AccountId=acc.Id));
        contacts.add(new Contact(LastName='Alfred', Contact_Type__c = 'Non-Customer', AccountId=acc.Id));
        INSERT contacts;
    }
    
    @isTest
    static void testFunctionalityOfHasCustomer(){
        List<Contact> contacts = [SELECT Id, LastName, Contact_Type__c FROM Contact];
        Account acc = [SELECT Has_Customer__c FROM Account];
        system.assert(!acc.Has_Customer__c, 'Expecting Has_Customer__c to be False, found otherwise');
        contacts[0].Contact_Type__c = 'Customer';
        contacts[1].Contact_Type__c = 'Customer';
        UPDATE contacts;
        acc = [SELECT Has_Customer__c FROM Account];
        system.assert(acc.Has_Customer__c, 'Expecting Has_Customer__c to be True, found otherwise');
        contacts[0].Contact_Type__c = 'Non-Customer';
        UPDATE contacts;
        acc = [SELECT Has_Customer__c FROM Account];
        system.assert(acc.Has_Customer__c, 'Expecting Has_Customer__c to be True, found otherwise');
        contacts[1].Contact_Type__c = 'Non-Customer';
        UPDATE contacts;
        acc = [SELECT Has_Customer__c FROM Account];
        system.assert(!acc.Has_Customer__c, 'Expecting Has_Customer__c to be False, found otherwise');
        
    }

}